![logo](https://github.com/bage79/nlp4kor/raw/master/ipynb/img/nlp4kor.png)

# nlp4kor
- http://github.com/bage79/nlp4kor
    - https://www.facebook.com/nlp4kor
- Natural Language Processing for Korean with Deep Learning (딥러닝을 이용한 한글 자연어 처리)
- 발표자료는 발표(행아웃) 후 공개되며, 동영상은 다음 발표전 까지 공개됩니다.
- 발표자료/동영상에 대한 오류 수정 및 피드백은 nlp4kor@gmail.com 로 주시면 최대한 반영하겠습니다.
- 소스 설치 및 실행에 대한 내용은 [INSTALL.md](https://github.com/bage79/nlp4kor/blob/master/INSTALL.md) 를 참조해주시기 바랍니다.

# License
- nlp4kor에서 자체 개발한 경우 (대부분의 경우)
  - [MIT License](https://github.com/bage79/nlp4kor/blob/master/LICENSE.txt)를 적용합니다.
  - 배포하지 않고 사용하는 경우, 아무런 제약을 받지 않습니다.
  - 무료 또는 상용 소프트웨어로 배포하시는 경우, README와 같은 주요 파일에 아래 내용을 포함하셔야 합니다.
```text
nlp4kor
https://github.com/bage79/nlp4kor
Copyright 2017 nlp4kor <nlp4kor@gmail.com>
```
- 외부의 소스코드 또는 데이터를 가져온 경우 (wikipedia corpus, MNIST...)
  - 각 디렉토리의 LICENSE.txt 를 따르시면 됩니다. 또한 라이센스에 대한 요약본인 LICENSE.md 도 참고하실 수 있습니다.

# SUMMARY 
- original: `http://yann.lecun.com/exdb/mnist/`
- data type: image (hand-written digits)
- [CC BY-SA License](http://creativecommons.org/licenses/by-sa/3.0/deed.ko)
		- MNIST의 라이센스가 상속됩니다. [CCL License](http://creativecommons.org/licenses/by-sa/3.0/deed.ko)
		
# `data/`
- input data (28x28 images & labels)

# `models/` tensorflow models
- `cnn/`: Convolutional Neural Network model
